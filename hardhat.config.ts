import { HardhatUserConfig } from 'hardhat/config';
import '@nomicfoundation/hardhat-toolbox';

const config: HardhatUserConfig = {
  solidity: {
    version: '0.8.17',
    settings: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  networks: {
    hardhat: {
      chainId: 1337,
      gas: 8000000,
    },
    devnet: {
      url: 'https://rpc.vnet.tenderly.co/devnet/my-first-devnet/1afd7ee5-8595-4bd7-a853-a305492eb142',
      chainId: 1,
    },
  },
};

export default config;
