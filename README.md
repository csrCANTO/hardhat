![png logo](https://user-images.githubusercontent.com/132018856/235363871-51cf3241-f2dc-4498-910f-594133662bce.png)

# CsrCanto contract sources

The wrapped $CANTO that shares the Contract Secured Revenue generated to its holders ❤️

---

CsrCanto has been deployed early to meet the Canto Online Hackathon deadline. While the underlying mechanism idea seems to be popular, the community came with important feedbacks that we would like to take in consideration for a proper final $csrCanto token deployment.

Therefore, this repository doesn't reflect the contract code already deployed. If you are interested about the current deployed contract, you can read its source directly from the canto explorers.

## Links

- [Docs](https://csrcanto.gitbook.io/csrcanto/)
- [Deployed contract](https://tuber.build/token/0xe73191C7D3a47E45780c76cB82AE091815F4C8F9/token-transfers)

## How to enable $csrCanto CSR revenue for your contract

By default, $csrCanto allows only EOAs to register in the claimers list. Although, if you plan to deploy a contract that would hold $csrCanto, you can still enjoy the benefits of $csrCanto revenue:

1. Talk with us. We can agree to add your contract in the claimers list.
2. Implement the following calls in your contract:

Get your fractional $CANTO reward from the CSR:

```solidity
function withdrawClaimed() public;
```

The amount of $CANTO eligible for your contract to claim:

```solidity
function getAmountClaimable(address addr) public view returns(uint256);
```

You should be able to call `withdrawClaimed()` when the following function returns `0`:

```solidity
function getRemainingTimeBeforeCanClaim(address addr) public view returns(uint256);
```

## $csrCANTO v2

CsrCanto has been deployed early to meet the Canto Online Hackathon deadline. While the underlying mechanism idea seems to be popular, the community came with important feedbacks that we would like to take in consideration for a proper final $csrCanto token deployment.

Therefore, this repository doesn't reflect the contract code already deployed. If you are interested about the current deployed contract, you can read its source [here](https://tuber.build/address/0xe73191C7D3a47E45780c76cB82AE091815F4C8F9/contracts#address-tabs).

## Interfaces

User:

```solidity
function deposit() public payable;           // wrap CANTO to csrCANTO
function withdraw(uint256 amount) public;    // unwrap csrCANTO to CANTO
function withdrawClaimed() public;           // collect the claimable CANTO from the CSR
function getAmountClaimable(address addr) public view returns(uint256); // Get claimable amount for account
function getClaimDelay() public view returns(uint256)
```

Admin:

```solidity
function setClaimDelay(uint256 delay) public onlyOwner; // Set minimum delay between claims
function withdrawFee(uint256 amount) public onlyOwner;  // emergency withdraw CANTO from the CSR
```

## Tests

```
$ npm install
```

Run your local testnet

```
$ npx hardhat node
```

Run the tests

```
$ npx hardhat test --network localhost
```
