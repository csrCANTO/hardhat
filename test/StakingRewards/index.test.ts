import { ethers } from 'hardhat';
import { expect } from 'chai';
import { Contract, utils, ContractFactory, constants } from 'ethers';

describe('StakingRewards', async () => {
  let StakeToken: Contract;
  let CSC: Contract;
  let StakingRewardsContract: Contract;
  let owner: ethers.SignerWithAddress;

  before('deploy contracts', async () => {
    const [rewardsDistribution, _owner] = await ethers.getSigners();
    owner = _owner;
    const cscFactory: ContractFactory = await ethers.getContractFactory('CSC');
    CSC = await cscFactory.connect(owner).deploy('CSC', '$CSC');
    await CSC.deployed();

    const mockContract: ContractFactory = await ethers.getContractFactory('CSC');
    StakeToken = await mockContract.connect(owner).deploy('StakeToken', '$ST');
    await StakeToken.deployed();

    const stakingRewardsContractFactory: ContractFactory = await ethers.getContractFactory(
      'StakingRewards'
    );
    StakingRewardsContract = await stakingRewardsContractFactory
      .connect(owner)
      .deploy(rewardsDistribution.address, CSC.address, StakeToken.address);
    await StakingRewardsContract.deployed();
  });

  before('mint CSC tokens', async () => {
    await CSC.mint(owner.address, utils.parseEther('10'));
    await StakeToken.mint(owner.address, utils.parseEther('10'));
  });

  it('should increase the total supply if token is staked', async () => {
    await StakeToken.approve(StakingRewardsContract.address, constants.MaxUint256);
    await StakingRewardsContract.stake(ethers.utils.parseEther('1'));
    const totalSupply = await StakingRewardsContract.totalSupply();
    expect(totalSupply).to.eq(utils.parseEther('1').toString());
  });

  it('should increase balance if StakeToken was deposited', async () => {
    await StakeToken.approve(StakingRewardsContract.address, constants.MaxUint256);
    await StakingRewardsContract.stake(ethers.utils.parseEther('1'));
    const balanceOf = await StakingRewardsContract.balanceOf(owner.address);
    expect(balanceOf.toString()).to.eq(utils.parseEther('2').toString());
  });
});
