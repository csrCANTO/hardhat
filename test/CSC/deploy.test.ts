import { Contract, ContractFactory } from 'ethers';
import { expect } from 'chai';

describe('CSC', async () => {
  let contract: Contract;
  before('deploy', async () => {
    const CSC: ContractFactory = await ethers.getContractFactory('CSC');
    contract = await CSC.deploy('CSC', '$CSC');
  });

  it('name and symbol', async () => {
    expect(await contract.name()).to.be.string('CSC');
  });
});
