// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

/*
   _                
  | |                ██████  █████  ███    ██ ████████  ██████  
 / __) ___ ___ _ __ ██      ██   ██ ████   ██    ██    ██    ██ 
 \__ \/ __/ __| '__|██      ███████ ██ ██  ██    ██    ██    ██ 
 (   / (__\__ \ |   ██      ██   ██ ██  ██ ██    ██    ██    ██ 
  |_| \___|___/_|    ██████ ██   ██ ██   ████    ██     ██████  
*/

contract Fees {
    // event Register(address account);
    // event Claim(address indexed account, uint256 claimed);
    // event ContractReceived(address from, uint256 amount);
    // event PullFundsFromTurnstile(address from, uint256 amount);
    // /*================================
    // =            DATASETS            =
    // ================================*/
    // struct Holder {
    //     address payee;
    //     /* Token Holders CSR Payments */
    //     // claimed but not yet withdrawn funds for a user
    //     uint256 claimedFunds;
    //     // cumulative funds received which were already processed for distribution - by user
    //     uint256 processedFunds;
    // }
    // mapping(address => Holder) public holders;
    // mapping(address => address) public payees;

    // function _claimFunds(address _forAddress) internal {
    //     uint256 unprocessedFunds = _calcUnprocessedFunds(_forAddress);

    //     holders[_forAddress].processedFunds = receivedFunds;
    //     holders[_forAddress].claimedFunds += unprocessedFunds;
    // }

    // function _calcUnprocessedFunds(address _forAddress) internal view returns (uint256) {
    //     if (holders[_forAddress].payee == address(0)) return 0;
    //     return
    //         (balanceOf(_forAddress) * (receivedFunds - holders[_forAddress].processedFunds)) /
    //         claimersTotalSupply;
    // }
}
