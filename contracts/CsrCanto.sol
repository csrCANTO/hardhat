// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

/*
   _                
  | |                ██████  █████  ███    ██ ████████  ██████  
 / __) ___ ___ _ __ ██      ██   ██ ████   ██    ██    ██    ██ 
 \__ \/ __/ __| '__|██      ███████ ██ ██  ██    ██    ██    ██ 
 (   / (__\__ \ |   ██      ██   ██ ██  ██ ██    ██    ██    ██ 
  |_| \___|___/_|    ██████ ██   ██ ██   ████    ██     ██████  
*/

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

interface Turnstile {
    function register(address) external returns(uint256);
    function getTokenId(address _smartContract) external view returns (uint256);
    function withdraw(uint256 _tokenId, address _recipient, uint256 _amount) external returns (uint256);
    function balances(uint256 _tokenId) external view returns (uint256);
}

/// @title  $csrCANTO token
/// @author csrCANTO team
/// @notice This is a wrapped CANTO token with a mechanism for token holders to
///         receive a share of the CSR generated
/// @dev    An ERC-20 wrapper contract with an adaptation of ERC-1843 that
///         redistributes the CSR to the token holders
/// @custom:experimental This is an experimental contract.
contract CsrCanto is ERC20, ReentrancyGuard {
    Turnstile immutable turnstile;
    uint256 public immutable turnstileTokenId;

    /*=====================================
    =            CONFIGURABLES            =
    =====================================*/
    string private _name = "CSR (wrapped) Canto";
    string private _symbol = "csrCANTO";
    address public ADMIN_ROLE;
    address public MANAGER_ROLE;

    /*================================
    =            DATASETS            =
    ================================*/
    struct Holder {
        address payee;
        /* Token Holders CSR Payments */
        // claimed but not yet withdrawn funds for a user
        uint256 claimedFunds;
        // cumulative funds received which were already processed for distribution - by user
        uint256 processedFunds;
    }
    mapping(address => Holder) public holders;
    mapping(address => address) public payees;

    uint256 public claimersTotalSupply = 0;
    uint256 public receivedFunds; // cumulative funds received by this contract

    /*==============================
    =            EVENTS            =
    ==============================*/
    event Deposit(address indexed account, uint256 amount);
    event Withdraw(address indexed account, uint256 amount);
    event Register(address account);
    event Claim(address indexed account, uint256 claimed);
    event ContractReceived(address from, uint256 amount);
    event PullFundsFromTurnstile(address from, uint256 amount);

    /*=======================================================
    =        TOKEN HOLDERS CSR PAYMENTS MECHANISM           =
    =======================================================*/

    receive() external payable {
        require(msg.sender == address(turnstile), "accept receiving plain $CANTO transfers only from turnstile contract");
        emit ContractReceived(msg.sender, msg.value);
    }

    function _claimFunds(address _forAddress) internal {
        uint256 unprocessedFunds = _calcUnprocessedFunds(_forAddress);

        holders[_forAddress].processedFunds = receivedFunds;
        holders[_forAddress].claimedFunds += unprocessedFunds;
    }

    function _calcUnprocessedFunds(address _forAddress) internal view returns (uint256) {
        if(holders[_forAddress].payee == address(0)) return 0;
        return
            balanceOf(_forAddress) 
            * ( receivedFunds - holders[_forAddress].processedFunds )
            / claimersTotalSupply;
    }

    /*
     * ERC20 overrides 
     */

    function transfer(address _to, uint256 _value)
        public override
        returns (bool)
    {
        _claimFunds(msg.sender);
        _claimFunds(_to);

        return super.transfer(_to, _value);
    }
    function transferFrom(address _from, address _to, uint256 _value)
        public override
        returns (bool)
    {
        _claimFunds(_from);
        _claimFunds(_to);

        return super.transferFrom(_from, _to, _value);
    }

    function _afterTokenTransfer (
        address from,
        address to,
        uint256 amount
    ) internal virtual override {
        if(holders[from].payee != address(0)) claimersTotalSupply = claimersTotalSupply - amount;
        if(holders[to].payee != address(0)) claimersTotalSupply = claimersTotalSupply + amount;
    }

    /* 
     * public functions 
     */
    
    /// @notice Pull and distribute the CSR among token holders
    /// @dev    Pull CANTO from the turnstile contract and register the funds by
    ///         incrementing the receivedFunds state
    function pullFundsFromTurnstile() public nonReentrant {
        uint256 amount = turnstile.balances(turnstileTokenId);
        require(amount > 0, "no funds to pull from turnstile");
        turnstile.withdraw(turnstileTokenId, payable(address(this)), amount);
        _mint(address(this), amount);
        receivedFunds = receivedFunds + amount; // register funds
        emit PullFundsFromTurnstile(msg.sender, amount);
    }

    /// @notice Get the amount of claimed funds available for withdrawal for a given 
    ///         account
    /// @dev    The amount of claimed funds available is calculated from the CSR
    ///         that has been received by the contract
    /// @param  _forAddress     The address to check
    /// @return The amount of claimed funds available for withdrawal by `_forAddress`
    function availableFunds(address _forAddress) public view returns(uint256) {
        return _calcUnprocessedFunds(_forAddress) + holders[_forAddress].claimedFunds;
    }

    /*=======================================
    =            PUBLIC FUNCTIONS           =
    =======================================*/

    /// @dev Set admin/manager roles, register the contract with the turnstile and store its NFT ID
    /// @param turnstileAddr The address of the turnstile contract
    constructor(address turnstileAddr) ERC20(_name, _symbol) {
        ADMIN_ROLE = msg.sender;
        MANAGER_ROLE = msg.sender;

        turnstile = Turnstile(turnstileAddr);
        turnstile.register(address(this));
        turnstileTokenId = turnstile.getTokenId(address(this));
    }

    function name() public view override returns (string memory) { return _name; }
    function symbol() public view override returns (string memory) { return _symbol; }
    
    /// @notice Wrap CANTO into $csrCANTO
    /// @dev Deposit CANTO and mint $csrCANTO, then claim the funds pulled from the turnstile
    function deposit() public payable {
        _mint(msg.sender, msg.value);
        _claimFunds(msg.sender);
        emit Deposit(msg.sender, msg.value);
    }

    /// @notice Unwrap $csrCANTO into CANTO
    /// @dev Withdraw CANTO and burn $csrCANTO
    /// @param amount The amount of $csrCANTO to withdraw
    function withdraw(uint256 amount) public nonReentrant {
        require(this.balanceOf(msg.sender) >= amount, "insufficient balance");

        _burn(msg.sender, amount);
        payable(msg.sender).transfer(amount);

        emit Withdraw(msg.sender, amount);
    }

    /// @notice Register to be able to claim Turnstile revenue (EOA only)
    /// @dev    Update the claimersTotalSupply state used to calculate the unprocessed
    //          funds pull from the Turnstile contract (CSR)
    function register() public {
        require(tx.origin == msg.sender, "Registrant must be an EOA");
        require(holders[msg.sender].payee == address(0), "Already registered as a claimer");

        holders[msg.sender].payee = msg.sender;
        payees[msg.sender] = msg.sender;
        claimersTotalSupply = claimersTotalSupply + balanceOf(msg.sender);
        
        emit Register(msg.sender);
    }

    /// @notice Check if an address is registered as a claimer
    /// @param addr The address to check
    /// @return True if the address is registered as a claimer, false otherwise
    function isClaimer(address addr) public view returns(bool) {
        return holders[addr].payee != address(0);
    }
    
    /// @notice Withdraw the CSR funds claimed internally at user's $csrCANTO balance change
    /// @dev    Withdraw the available funds, reset user states related to their funds 
    ///         and burn the $csrCANTO equivalent
    function withdrawClaimed() public nonReentrant {
        require(claimersTotalSupply > 0, "no claimers yet");
        require(payees[msg.sender] != address(0), "not in the payees list");

        Holder storage holder = holders[payees[msg.sender]];
        uint256 withdrawnClaim = availableFunds(payees[msg.sender]);
        holder.processedFunds = receivedFunds;
        holder.claimedFunds = 0;

        payable(msg.sender).transfer(withdrawnClaim);

        _burn(address(this), withdrawnClaim);

        emit Claim(msg.sender, withdrawnClaim);
    }

    /*=======================================
    =            ADMIN FUNCTIONS            =
    =======================================*/
    function a_setAdminRole(address addr) public onlyAdmin { 
        require(addr != address(0), "0x00... cannot be Admin");
        ADMIN_ROLE = addr;
    }
    function a_setManagerRole(address addr) public onlyAdmin { 
        require(addr != address(0), "0x00... cannot be Manager");
        MANAGER_ROLE = addr;
    }
    function a_setName(string calldata __name) public onlyAdmin { _name = __name; }
    function a_setSymbol(string calldata __symbol) public onlyAdmin { _symbol = __symbol; }

    /*=======================================
    =            MANAGER FUNCTIONS          =
    =======================================*/
    function m_addClaimer(address addr, address payee) public onlyManager {
        require(holders[addr].payee == address(0), "address already in the claimers list");
        holders[addr].payee = payee;
        payees[payee] = addr;
        claimersTotalSupply += balanceOf(addr);
    }
    function m_delClaimer(address addr) public onlyManager {
        require(holders[addr].payee != address(0), "address already not claimer");
        delete holders[addr].payee;
        delete payees[addr];
        claimersTotalSupply -= balanceOf(addr);
    }

    /*==============================
    =          MODIFIERS           =
    ==============================*/
    modifier onlyAdmin() {
        require(msg.sender == ADMIN_ROLE);
        _;
    }
    modifier onlyManager() {
        require(msg.sender == MANAGER_ROLE);
        _;
    }
}