// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

/*
   _                
  | |                ██████  █████  ███    ██ ████████  ██████  
 / __) ___ ___ _ __ ██      ██   ██ ████   ██    ██    ██    ██ 
 \__ \/ __/ __| '__|██      ███████ ██ ██  ██    ██    ██    ██ 
 (   / (__\__ \ |   ██      ██   ██ ██  ██ ██    ██    ██    ██ 
  |_| \___|___/_|    ██████ ██   ██ ██   ████    ██     ██████  
*/

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./utils/Math.sol";

contract CsrUSD is ERC20, Ownable, Math {
    mapping(address => uint256) private _balances;
    mapping(address => mapping(address => uint256)) private _allowances;
    uint256 private _totalSupply;

    constructor(string memory name, string memory symbol) ERC20(name, symbol) {}

    function mint(address user, uint amount) external onlyOwner {
        _balances[user] = add(balanceOf(user), amount);
        _totalSupply = add(_totalSupply, amount);
        emit Transfer(address(0), user, amount);
    }

    function burn(address user, uint amount) external onlyOwner {
        require(balanceOf(user) >= amount, "insufficient balance");
        if (user != msg.sender && _allowances[user][msg.sender] != type(uint).max) {
            require(_allowances[user][msg.sender] >= amount, "insufficient allowance");
            _allowances[user][msg.sender] = sub(_allowances[user][msg.sender], amount);
        }
        _balances[user] = sub(balanceOf(user), amount);
        _totalSupply = sub(_totalSupply, amount);
        emit Transfer(user, address(0), amount);
    }
}
