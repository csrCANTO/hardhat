// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.17;

/*
   _                
  | |                ██████  █████  ███    ██ ████████  ██████  
 / __) ___ ___ _ __ ██      ██   ██ ████   ██    ██    ██    ██ 
 \__ \/ __/ __| '__|██      ███████ ██ ██  ██    ██    ██    ██ 
 (   / (__\__ \ |   ██      ██   ██ ██  ██ ██    ██    ██    ██ 
  |_| \___|___/_|    ██████ ██   ██ ██   ████    ██     ██████  
*/

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./utils/Math.sol";

interface Turnstile {
    function getTokenId(address _smartContract) external view returns (uint256);
    function assign(uint256 _tokenId) external returns (uint256);
}

contract Vault is Ownable, Math {
    Turnstile immutable turnstile;
    IERC20 csrCANTO;

    mapping(address => uint256) public collateral;

    constructor(address _csrCANTO, address turnstileAddr) {
        csrCANTO = IERC20(_csrCANTO);

        turnstile = Turnstile(turnstileAddr);
        uint256 turnstileTokenId = turnstile.getTokenId(_csrCANTO);
        turnstile.assign(turnstileTokenId);
    }

    function createVault(uint256 amount) external {
        require(collateral[msg.sender] == 0, 'vault already created');
        csrCANTO.transferFrom(msg.sender, address(this), amount);
        collateral[msg.sender] = amount;
    }

    function withdrawCollateral(uint256 amount) external {
        // require(collateral[msg.sender] == msg.sender, 'not owner');
        csrCANTO.transfer(msg.sender, amount);
        collateral[msg.sender] = sub(collateral[msg.sender], amount);
    }

    function depositCollateral(uint256 amount) external {
        // require(collateral[msg.sender] == msg.sender, 'not owner');
        csrCANTO.transferFrom(msg.sender, address(this), amount);
        collateral[msg.sender] = add(collateral[msg.sender], amount);
    }

    // function liquidate(address owner) external {

    // }
}
